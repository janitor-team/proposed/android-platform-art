NAME = dexdump
SOURCES = dexdump_cfg.cc dexdump_main.cc dexdump.cc
SOURCES := $(foreach source, $(SOURCES), dexdump/$(source))
CXXFLAGS +=
CPPFLAGS += \
  -Idexdump \
  -Iruntime \
  -I/usr/include/android/nativehelper

# libsigchain defines wrapper functions around sigaction() family. In order to
# override the ones provided by libc, libsignal must appear in the shared
# object dependency tree before libc in the breadth-first order.
LDFLAGS += -nodefaultlibs 
LIBRARIES_FLAGS += -lsigchain -lc -lstdc++ -lgcc_s

LDFLAGS += \
  -L/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -Ldebian/out \
  -Wl,-rpath-link=debian/out \
  -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android
LIBRARIES_FLAGS += \
  -lart \
  -lbase

debian/out/$(NAME): $(SOURCES)
	mkdir --parents debian/out
	clang++ $^ -o $@ $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $(LIBRARIES_FLAGS)