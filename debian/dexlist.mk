NAME = dexlist
SOURCES = dexlist.cc
SOURCES := $(foreach source, $(SOURCES), dexlist/$(source))
CPPFLAGS += \
  -Iruntime \
  -I/usr/include/android/nativehelper

# See the comment in `dexdump.mk`
LDFLAGS += -nodefaultlibs
LIBRARIES_FLAGS += -lsigchain -lc -lstdc++ -lgcc_s

LDFLAGS += \
  -L/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -Ldebian/out \
  -Wl,-rpath-link=debian/out \
  -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android
LIBRARIES_FLAGS += -lart

debian/out/$(NAME): $(SOURCES)
	mkdir --parents debian/out
	clang++ $^ -o $@ $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $(LIBRARIES_FLAGS)